import subprocess
import time

def execute():
    for r,i in ins:
        for t,km in ts:
            for vl in vls:
                k = km + "000000"
                s = template.format(t=t,r=r,i=i,k=k,km=km,vl=vl,comp=comp)
                f = open("scripts/sub.sh","w")
                f.write(s)
                f.close()
                p = subprocess.Popen(["sbatch","scripts/sub.sh"],stdout=subprocess.PIPE)
                out,err = p.communicate()
                if out != None:
                    print out,
                if err != None:
                    print err,

template = """#!/bin/bash
#SBATCH --time=01:00:00
#SBATCH --exclusive
#SBATCH --output=m_{t}_{i}_{r}_k{km}_sve{vl}_{comp}_%j.perf
#SBATCH --error=m_{t}_{i}_{r}_k{km}_sve{vl}_{comp}_%j.txt

time ./setvl {vl} ./bwa-mem2/bin/bwa-mem2-{comp} mem -t {t} -1 -K {k} -o /dev/null inputs-comp3/{r}.fa inputs/{i}.fasta
"""

#ins=[("ffg","F1"),("ffg","F2"),("ffg","F3")]
ins=[("hg38","D3"),("hg38","D4"),("hg38","D5")]
ts=[("1","10"),("8","80"),("16","160"),("32","320"),("48","480"),("64","640")] # threads,k(M)
vls=["128","256"]
versions = "base inline popcnt nseqs2 nseqs4 nseqs8 pred".split()
for v in versions:
    comp=v
    execute()

