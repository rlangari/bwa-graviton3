import sys
import os
import re
from parse_times import get_params

path = "."

if len(sys.argv) > 1:
    path = sys.argv[1]

def parse():
    d={}

    for vl in vls:
        for k in ks:
            for comp in comps:
                for inp in inputs:
                    key = c+"_"+inp+"_sve"+vl+"_k"+k+"_"+comp
                    for s in phases:
                        d[key+"_"+s]={}
                    for filename in os.listdir(path):
                        if re.match("m_"+c+"_"+inp+".*_k"+k+"_sve"+vl+"_"+comp+".*perf",filename):
                            with open(path+"/"+filename,"r") as f:
                                for line in f:
                                    if ":" not in line:
                                        continue
                                    s = line.split(":")
                                    name = s[0].strip()
                                    num = s[1].strip()
                                    for s in phases:
                                        if name not in d[key+"_"+s]:
                                            d[key+"_"+s][name] = num
                                            break

    for key in sorted(d.itervalues().next()):
        print ","+key,
    print ""

    for vl in vls:
        for k in ks:
            for inp in inputs:
                for s in phases:
                    print c+"_"+inp+"_sve"+vl+"_k"+k+"_"+s,
                    for comp in comps:
                        key = c+"_"+inp+"_sve"+vl+"_k"+k+"_"+comp+"_"+s
                        for key2,value in sorted(d[key].iteritems()):
                            print ","+value,
                    print ""

phases=["SMEM","SAL","BSW"]
inputs=["D3","D4","D5"]
vls=["128","256"]
comps = "base inline popcnt nseqs2 nseqs4 nseqs8 pred".split()

for c in "1 8 16 32 48 64".split():
    ks=[c+"0"]
    parse()

