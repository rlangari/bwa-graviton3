import sys
import os
import re

def get_params(path):
    comps = []
    vls = []
    for filename in os.listdir(path):
        if re.match("m_.*",filename):
            comp = filename.split("_")[6].split(".")[0]
            if comp not in comps:
                comps.append(comp)
            vl = filename.split("_")[5][3:]
            if vl not in vls:
                vls.append(vl)
    vls.sort(key=int)
    return vls,comps

def parse():
    size = len(inputs)*len(phases)
    d=[]
    i=0

    for vl in vls:
        for k in ks:
            for comp in comps:
                print(c + "_" + vl + "_k" + k + "_" + comp),
                for inp in inputs:
                    d=[]
                    for filename in os.listdir(path):
                        if re.match("m_"+c+"_"+inp+".*_k"+k+"_sve"+vl+"_"+comp+".*err",filename):
                            with open(path+"/"+filename,"r") as f:
                                for line in f:
                                    if "SMEM compute avg" in line or "SAL compute avg" in line or "BSW time, avg" in line:
                                        s = line.split(":")[1].split(",")[0].strip()
                                        d.append(s)
                                        print(","+s),
                    if len(d) == 3:
                        s = float(d[-1])+float(d[-2])+float(d[-3])
                    else:
                        s = "0,0,0,0"
                    print(","+str(s)),
                print("")

if __name__ == "__main__":
    path = "."

    if len(sys.argv) > 1:
        path = sys.argv[1]

    phases=["SMEM","SAL","SW","TOTAL"]
    inputs = ["D3","D4","D5"]
    vls,comps = get_params(path)

    if __name__=="__main__":
        print("hg38")
        c="1"
        ks=["10"]
        parse()
        c="12"
        ks=["120"]
        parse()
        c="24"
        ks=["240"]
        parse()
        c="48"
        ks=["480"]
        parse()

